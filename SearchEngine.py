"""__author__ = "Ghaith Rawi"
__copyright__ = "Copyright (C) 2020 Datacom"
__license__ = "Private Use - Restricted"
__version__ = "1.0"
"""
# Habitat and PureCloud ,
# No need to search for Habitat contacts in PC (API) as its slow and expensive

''' Standard Contact Format - Habitat & PC
"firstName": "",    (required)
"lastName": "",     (required)
"organization": "", (required)
"workPhone":"",
"cellPhone":"",
"homePhone":"",
"otherPhone":"",
"workEmail": "",
"personalEmail": "",
"otherEmail": "",
"address":""
'''


def search_contact_in_list(contact, contact_list):
    found = False
    for cont in contact_list:
        if str(cont["firstName"]).lower() == str(contact.firstName).lower() and \
                str(cont["lastName"]).lower() == str(contact.lastName).lower():
            found = True
        elif str(cont["workEmail"]).lower() == str(contact.workEmail).lower() \
                or str(cont["personalEmail"]).lower() == str(contact.personalEmail).lower() or \
                str(cont["otherEmail"]).lower() == str(contact.otherEmail).lower():
            found = True
        elif cont["workPhone"] == contact.workPhone or cont["cellPhone"] == contact.cellPhone or \
                cont["homePhone"] == contact.homePhone or cont["otherPhone"] == contact.otherPhone:
            found = True
        else:
            found = False

    return found
