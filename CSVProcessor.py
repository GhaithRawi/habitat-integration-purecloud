"""__author__ = "Ghaith Rawi"
__copyright__ = "Copyright (C) 2020 Datacom"
__license__ = "Private Use - Restricted"
__version__ = "1.0"
__Update Logs__ :
28-8-2020 : updated process_main_csv, Added new data filed (Contact Other Phone)
25-8-2020 : updated process_main_csv, sniffed CSV file as format has changed
"""

import pandas as pd
import os
import csv


def clean_number(input_number):
    numb = str(input_number)    
    number = numb.replace(' ', '')
    if '.' in number:
        number = numb.split(".")[0]
    if number.isdecimal():  # Ensure E.164 - format        
        if number.startswith('02'):
            number = number.replace('02', '+612', 1)
        elif number.startswith('03'):
            number = number.replace('03', '+613', 1)
        elif number.startswith('04'):
            number = number.replace('04', '+614', 1)
        elif number.startswith('07'):
            number = number.replace('07', '+617', 1)
        elif number.startswith('08'):
            number = number.replace('08', '+618', 1)
        elif number.startswith('09'):
            number = number.replace('0', '+618', 1)
        # E.164 - format 2
        elif number.startswith('2'):
            number = number.replace('2', '+612', 1)
        elif number.startswith('3'):
            number = number.replace('3', '+613', 1)
        elif number.startswith('4'):
            number = number.replace('4', '+614', 1)
        elif number.startswith('7'):
            number = number.replace('7', '+617', 1)
        elif number.startswith('8'):
            number = number.replace('8', '+618', 1)
        elif number.startswith('9'):
            number = '+618' + number       
        else:
            number = ""

        return number

    else:        
        return ''


# Full path , Example: 'data/db_extract/contacts_20200608.csv'
# File name/Date will be confirmed before being passed to this function
# returns a list of file names which has been writen to disk (chunks list)
def process_main_csv(csv_file_name, yesterday_date, logger):
    # Process CSV From DB Extract
    # Generate a list of all csv files after dividing the main csv and return it back to be uploaded to PC
    chunk_file_list = []
    # print(csv_file_name) MUST BE FULL PATH
    if yesterday_date == '':
        logger.log_message('ERROR', 'Yesterday date was not generated successfully, Check System Date setting')

    with open(csv_file_name) as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.read(14734))

    try:
        df = pd.read_csv(csv_file_name, engine='python', dialect=dialect, error_bad_lines=False,
                         index_col='CustomerCode')

        logger.log_message('INFO', f'Main contacts file was loaded Successfully from db_extract/PROD, '
                                   f'File Size = {(os.stat(csv_file_name).st_size / 1000000)} MB')

        # df = df[['FirstName', 'MiddleName', 'Surname', 'Title', 'WorkEmailAddress', 'EmailAddress', 'WorkContactNo',
        #         'HomeContactNo', 'MobileContactNo', 'Address', 'ModifiedDate', 'ActiveCustomerIndicator']]

        df_modified = df.copy()
        # clear Memory
        del df
        df_modified = df_modified.rename(columns={'Title': 'Contact Title', 'FirstName': 'Contact First Name',
                                                  'MiddleName': 'Contact Middle Name',
                                                  'Surname': 'Contact Last Name',
                                                  'WorkEmailAddress': 'Contact Work Email',
                                                  'EmailAddress': 'Contact Personal Email',
                                                  'WorkContactNo': 'Contact Work Phone',
                                                  'HomeContactNo': 'Contact Home Phone',
                                                  'MobileContactNo': 'Contact Cell Phone',
                                                  'OtherContactNo': 'Contact Other Phone',
                                                  'Address': 'Contact Address 1', 'ActiveCustomerIndicator': 'Active'})
        # Rename Index
        df_modified.index.names = ['Contact Id']

        df_modified['Account Id'] = 1
        df_modified['Account External System UR'] = ''
        df_modified['Account Name'] = 'Habitat'

        df_modified.fillna("", inplace=True)
        

        df_modified['Contact Work Email'] = df_modified['Contact Work Email'].str.lower()
        df_modified['Contact Personal Email'] = df_modified['Contact Personal Email'].str.lower()

        df_modified['Contact Work Phone'] = df_modified['Contact Work Phone'].apply(clean_number)
        df_modified['Contact Home Phone'] = df_modified['Contact Home Phone'].apply(clean_number)
        df_modified['Contact Cell Phone'] = df_modified['Contact Cell Phone'].apply(clean_number)
        df_modified['Contact Other Phone'] = df_modified['Contact Other Phone'].apply(clean_number)

        filtered_data = (df_modified['Contact Work Email'] == "") & (df_modified['Contact Personal Email'] == "")
        filtered_data2 = (df_modified['Contact Work Phone'] == "") & (df_modified['Contact Home Phone'] == "") & (
                df_modified['Contact Cell Phone'] == "") & (df_modified['Contact Other Phone'] == "") 

        df_modified = df_modified[~ (filtered_data & filtered_data2)]

        # Generate a list of inactive users, Data to be removed (NOTE: Do this before you filter df_modified ,
        # otherwise no data ). NOTE: this should remove all contacts which were modified a day ago (Yesterday)
        df_to_be_deleted = df_modified.copy()
        df_to_be_deleted = df_to_be_deleted[
            (df_to_be_deleted['Active'] != "Y") & (df_to_be_deleted['ModifiedDate'] == yesterday_date)]
        df_to_be_deleted = df_to_be_deleted.drop(
            ['Contact Title', 'Contact Middle Name', 'Account Id',
             'Account External System UR', 'Account Name'], axis=1)

        # Remove all Inactive Users
        df_modified = df_modified[df_modified['Active'] == "Y"]
        df_modified = df_modified.drop(['Active'], axis=1)

        df_to_write = df_modified
        if df_to_write.shape[0] != '':
            logger.log_message('INFO', 'Main data extract was processed successfully')

        logger.log_message('INFO', f'Checking all records which were modified yesterday {yesterday_date}')
        if df_to_be_deleted.index.size > 0:
            # Write inactive users data to Disk (path : data/delete/)
            deleted_file_name = 'data/delete/deleted.csv'
            try:
                logger.log_message('INFO', 'Writing inactive users data to File, Name : ' + deleted_file_name)
                df_to_be_deleted.to_csv(deleted_file_name)
                if (os.stat(deleted_file_name).st_size / 1000000) > 0:
                    logger.log_message('INFO', 'Inactive users data was saved to Disk Successfully')

            except PermissionError:
                logger.log_message('ERROR', 'Error Writing Data to Disk, please check Permission')
            except FileNotFoundError:
                logger.log_message('ERROR', 'Error Writing Data to Disk, \"delete\" Folder was not found')

        else:
            logger.log_message('ERROR', 'No inactive contacts to be deleted')

        if df_to_write.index.size > 0:  # Only proceed when there is data to write
            logger.log_message('INFO', 'Trying to divide Main Data file into multiple 10MB Chunks')

            # Divide Main CSV into Chunks (below 10MB each) and write new files to Disk to be uploaded to PC
            max_file_rows = df_to_write.index.size
            # 100k Rows by default ~ 10MB or more depending on data
            chunk_size = 100000
            rows_from = 0
            rows_to = chunk_size
            file_n = 1

            while True:
                logger.log_message('INFO', '\n')
                logger.log_message('INFO', 'Total rows to Write = %s ' % str(max_file_rows))
                logger.log_message('INFO', 'Writing rows from: ' + str(rows_from) + ' To: ' + str(rows_to))

                df_to_write = df_modified.iloc[rows_from:rows_to]
                file_name_i_no_path = 'contacts_%s.csv' % str(file_n)
                file_name_i = 'data/upload/' + 'contacts_%s.csv' % str(file_n)

                try:
                    logger.log_message('INFO', 'Writing Data to File, Name : ' + file_name_i)
                    df_to_write.to_csv(file_name_i)
                    if (os.stat(file_name_i).st_size / 1000000) > 9.7:
                        logger.log_message('INFO', 'File size is bigger than limit (current = %s MB), '
                                                   'reducing chuck size by 20k rows, Trying Again...'
                                           % str((os.stat(file_name_i).st_size / 1000000)))

                        # reduce file size to limit below 10MB (acceptable by Genesys Cloud), Reducing by 20K
                        # Rows each time
                        rows_to -= 20000
                        df_to_write = df_modified  # Reload DF
                        # try again
                        continue

                    logger.log_message('INFO', '\nSuccessfully wrote %s rows ' % str(rows_to))
                    logger.log_message('INFO', 'New file Size = %s' % str((os.stat(file_name_i).st_size / 1000000)))

                    if rows_to > max_file_rows:
                        # Debug
                        # print('Max rows = %s ' % str(max_file_rows))
                        # print(rows_to)
                        chunk_file_list.append(file_name_i_no_path)  # Add last file name before exiting
                        logger.log_message('INFO', 'Finished Writing All Data to Dissk... \n')
                        break

                    # Ok, file was written, add that name to the files list
                    chunk_file_list.append(file_name_i_no_path)
                    rows_from = rows_to
                    rows_to = rows_from + chunk_size
                    file_n += 1
                except PermissionError:
                    logger.log_message('ERROR', 'Error Writing Data to Disk, please check Permission')
                    logger.log_message('ERROR', 'Abort...')
                    break

        else:
            logger.log_message('ERROR', f'There is no data to write, please check your filters on main csv file, '
                                        f'Passed df_to_write index size = {df_to_write.index.size}')

    except FileNotFoundError:
        logger.log_message('ERROR', f'Error Loading Main csv file : {csv_file_name} , File Not Found, CHECK PATH')
        logger.log_message('ERROR', 'Abort...')

    except ValueError:
        logger.log_message('ERROR', f'Error Loading Main csv file : {csv_file_name} , Invalid Column Names')
        logger.log_message('ERROR', 'Abort...')

    # Finally, return the files list to caller
    logger.log_message('ERROR', f'Generated File List = {chunk_file_list}')
    return chunk_file_list

