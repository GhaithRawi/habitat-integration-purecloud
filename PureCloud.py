"""__author__ = "Ghaith Rawi"
__copyright__ = "Copyright (C) 2020 Datacom"
__license__ = "Private Use - Restricted"
__version__ = "1.0"
-- Last update : 18-6-2020
"""

import base64
import requests
from FilesManager import FilesManager

"""
Required Permissions:
External Contacts > External Entity > Import
External Contacts > Contact > View
External Contacts > Contact > Delete
"""


class PC:
    def __init__(self):
        # add OAuth Client credentials from PC, No need to save this in environment for now
        self.region = "https://login.mypurecloud.com.au"
        self.api_url = "https://api.mypurecloud.com.au"
        self.bulk_upload_api = "https://apps.mypurecloud.com.au"
        self.client_id = "8ccb70e9-abd4-40c7-ae48-87009041f4a0"
        self.client_secret = "gX7rmj6M83JS-1NlopsxorhJ3Ix_je7P_tpEcWNm49w"

    def get_pc_token(self, logger):
        # Base64 encode the client ID and client secret
        authorization = base64.b64encode(bytes(self.client_id + ":" + self.client_secret, "ISO-8859-1")).decode("ascii")

        # Prepare for POST /oauth/token request
        request_headers = {
            "Authorization": f"Basic {authorization}",
            "Content-Type": "application/x-www-form-urlencoded"
        }
        request_body = {
            "grant_type": "client_credentials"
        }

        # Get token from PC (Sydney)
        try:
            response = requests.post(self.region + "/oauth/token", data=request_body, headers=request_headers)
            # Check response
            if response.status_code == 200:
                logger.log_message('INFO', 'Access Token Retrieved Successfully from PC')
                # Debug
                # print("Got token")
                # print(response.json())
                # print(response.json().get('token_type'))
                # print(response.json().get('access_token'))
                token = (response.json().get('access_token'), response.json().get('token_type'))
                logger.log_message('INFO', f'TOKEN : {token}')
                return token
            else:
                # print(f"Failure: {str(response.status_code)} - {response.reason}")
                raise ValueError('Error while Generating PC Token, Please Check Client Credentials : get_pc_token() ')

        except ValueError as e:
            # print(e)
            logger.log_message('ERROR', e)
            return 'error', 'error'

    def get_external_contacts(self, access_token, logger):
        # required Permission : externalContacts:contact:view
        # print(access_token)
        url = self.api_url + "/api/v2/externalcontacts/contacts"
        # print(url)
        requestheaders = {
            "Authorization": f"bearer {access_token}"
        }
        try:
            response = requests.get(url, headers=requestheaders)
            # Debug
            # print(response.json())
            if response.status_code == 200:
                logger.log_message('INFO', "External Contacts List Retrieved Successfully from PC")
                contacts = response.json()['entities']
                return contacts
            else:
                # print(f"Failure: {str(response.status_code)} - {response.reason}")
                raise ValueError(
                    'Error retrieving Contacts from PC,Please check Access Token : get_external_contacts()')

        except ValueError as e:
            # print(str(e))
            logger.log_message('ERROR', e)
            return [{"firstName": 'Error - Empty Contacts List'}]

    def bulk_upload_external_contacts_csv(self, source_csv_file, access_token, logger):
        # csv File size must not be bigger than 10MB
        # required Permission : External Contacts > External Entity > Import
        #                       External Contacts > Contact > View
        # print(access_token)
        url = self.bulk_upload_api + "/uploads/crm-sync"
        # print(url)
        source_csv_file = f'data/upload/{source_csv_file}'
        requestheaders = {
            "Authorization": f"bearer {access_token}"
        }
        request_data = {"filename": (source_csv_file, open(source_csv_file, 'rb'))}
        try:
            response = requests.post(url, headers=requestheaders, files=request_data)
            # Debug
            # print(response.json())
            if response.status_code == 200:
                logger.log_message('INFO', f"External Contacts List was uploaded Successfully to PC"
                                           f"from csv File {source_csv_file}")
                return True

            else:
                # print(f"Failure: {str(response.status_code)} - {response.reason}")
                raise ValueError(
                    f'Error Uploading External Contacts to PC from csv File {source_csv_file},'
                    f'Please check Access Token, csv file size (no more than 10MB), format '
                    f': bulk_upload_external_contacts_csv()')

        except ValueError as e:
            # print(str(e))
            logger.log_message('ERROR', e)
            return False
        return False

    def save_new_contacts(self, contacts_list, access_token):
        print(self.api_url)
        for contact_to_save in contacts_list:
            print(contact_to_save.firstName)

    def update_contacts(self, contacts_list, access_token):
        print(self.api_url)
        for contact_to_update in contacts_list:
            print(contact_to_update.firstName)

    def search_for_external_contact(self, contact, access_token, logger):
        keywords_list = contact['search_keywords_list']
        fst_name = contact['first_name']
        lst_name = contact['last_name']
        contact_id = ''
        main_url = self.api_url + "/api/v2/externalcontacts/contacts?q="
        # print(url)
        requestheaders = {
            "Authorization": f"bearer {access_token}"
        }

        try:
            if len(keywords_list) != 0:
                for keyword in keywords_list:  # search by all keywords until you find the contact
                    print(keyword)
                    url = main_url + keyword
                    # print(url)
                    response = requests.get(url, headers=requestheaders)
                    # Debug
                    # print(response.json())
                    if response.status_code == 200:
                        if response.json()['total'] == 1:
                            contact_id = response.json()['entities'][0]['id']
                            logger.log_message('INFO', f'External Contact \'{fst_name} ,{lst_name}\' found in PC using '
                                                       f'keyword \"{keyword}\", contact id returned = {contact_id}')
                            break
                        elif response.json()['total'] > 1:  # if duplicate, try another keyword
                            logger.log_message('INFO', f'Duplicate External Contact found in PC using keyword '
                                                       f'\"{keyword}\" trying to search with another keyword')
                            continue
                    else:
                        # print(f"Failure: {str(response.status_code)} - {response.reason}")
                        raise ValueError(
                            'Error retrieving Contact from PC,Please check Access Token')
            else:
                logger.log_message('ERROR', f'No Keywords were generated for this contact \'{fst_name} ,{lst_name}\', '
                                            f'cancel PC search {keywords_list}')
            return contact_id

        except ValueError as e:
            logger.log_message('ERROR',
                               f'Error retrieving Contact \'{fst_name} ,{lst_name}\' from PC,Please check Access Token {e}')
            return ''

    def delete_external_contact_by_id(self, contact_id, access_token, logger):
        # required Permission
        #  externalContacts:contact:delete
        url = self.api_url + f"/api/v2/externalcontacts/contacts/{contact_id}"
        # print(url)
        requestheaders = {
            "Authorization": f"bearer {access_token}"
        }

        try:
            response = requests.delete(url, headers=requestheaders)
            # Debug
            # print(response.json())
            if response.status_code == 200:
                logger.log_message('INFO', f'External Contact was deleted successfully in PC, id {contact_id}')
                return True
            else:
                # print(f"Failure: {str(response.status_code)} - {response.reason}")
                raise ValueError(
                    f'Error deleting Contact from PC,Please check Permission / Access Token {response}')

        except ValueError as e:
            logger.log_message('ERROR', e)
            return False

    def delete_contacts(self, access_token, logger):
        # 1- Generate keywords for all contacts in (DELETE) folder delete.csv,
        # for each contact, create a list of keywords (from fields like email,phone number or
        # address, etc) this is useful to search for contacts in Genesys Cloud
        # 2- search for contact in PC using hte keywords list,
        # 3- delete contact if found
        fm = FilesManager()
        # read contacts from delete.csv
        contacts_list = fm.generate_contacts_keywords_from_delete_csv(logger)

        # Search for contact in PC, using keywords
        if len(contacts_list) != 0:
            for contact in contacts_list:
                first_name = contact['first_name']
                last_name = contact['last_name']
                # print(contact)
                returned_contact_id = self.search_for_external_contact(contact, access_token, logger)
                # if contact was found, delete it
                if returned_contact_id != '':
                    self.delete_external_contact_by_id(returned_contact_id, access_token, logger)
                else:
                    logger.log_message('INFO', f'contact \'{first_name} ,{last_name}\' was not found in PC')
            logger.log_message('INFO', 'Completed deleting all contacts in (delete.csv)')
        else:
            logger.log_message('INFO', 'No contacts found to delete in (delete.csv)')
