"""__author__ = "Ghaith Rawi"
__copyright__ = "Copyright (C) 2020 Datacom"
__license__ = "Private Use - Restricted"
__version__ = "1.0"
-- Last update : 18-6-2020
"""

import logging


class Logger:
    def __init__(self):
        self.logger_id = 1

    def start_logger(self, today_date):
        today = today_date.strftime("%d-%m-%Y")
        self.self_pass()
        logs_file_name = f"data/logs/{today}.log"
        logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                            filename=logs_file_name, filemode='a', level=logging.INFO)

    def log_message(self, level, msg):
        self.self_pass()
        if level == 'INFO':
            logging.info(msg)
        elif level == 'ERROR':
            logging.error(msg)
        else:
            logging.warning(msg)

    def stop_logger(self):
        self.self_pass()
        logging.shutdown()

    def self_pass(self):
        pass
