"""__author__ = "Ghaith Rawi"
__copyright__ = "Copyright (C) 2020 Datacom"
__license__ = "Private Use - Restricted"
__version__ = "1.0"
-- Last update : 18-6-2020
"""

import os
from datetime import datetime, timedelta
import csv
import time


class FilesManager:
    def __init__(self):
        self.root_path = '.'
        self.data_extract_path = 'data/db_extract/PROD'
        self.upload_data_path = 'data/upload'
        self.trash_data_path = 'data/trash'
        self.error_data_path = 'data/error'
        self.delete_data_path = 'data/delete'
        self.retry_data_path = 'data/retry'
        self.logs_data_path = 'data/logs'
        self.logs_file_name = 'data/logs/System_Events.log'

        self.folders = {
            'root': '.',
            'extract': 'data/db_extract/PROD',
            'upload': 'data/upload',
            'trash': 'data/trash',
            'error': 'data/error',
            'delete': 'data/delete',
            'retry': 'data/retry',
            'logs': 'data/logs',

        }

    def create_folders_tree(self, logger):
        content_list = []
        completed = True
        for root, dirs, files in os.walk("data", topdown=True):
            for name in dirs:
                content_list.append(name)

        for folder in content_list:
            # check if a folder is not available, then create it
            if 'db_extract' not in content_list:
                try:
                    os.makedirs(self.data_extract_path)
                    logger.log_message('INFO', 'DB_EXTRACT Folder Created Successfully')
                except OSError as e:
                    logger.log_message('ERROR', 'Error while creating DB_EXTRACT Folder, Check '
                                                'Permission')
                    completed = False

            if 'delete' not in content_list:
                try:
                    os.mkdir(self.delete_data_path)
                    logger.log_message('INFO', 'DELETE Folder Created Successfully')
                except OSError as e:
                    logger.log_message('ERROR', 'Error while creating DELETE Folder, Check '
                                                'Permission')
                    completed = False

            if 'error' not in content_list:
                try:
                    os.mkdir(self.error_data_path)
                    logger.log_message('INFO', 'Error Folder Created Successfully')
                except OSError as e:
                    logger.log_message('ERROR', 'Error while creating ERROR Folder, Check '
                                                'Permission')
                    completed = False

            # if 'retry' not in content_list:
            #     try:
            #         os.mkdir(self.retry_data_path)
            #         logger.log_message('INFO', 'RETRY Folder Created Successfully')
            #     except OSError as e:
            #         logger.log_message('ERROR', 'Error while creating RETRY Folder, Check '
            #                                     'Permission')
            #         completed = False

            # if 'trash' not in content_list:
            #     try:
            #         os.mkdir(self.trash_data_path)
            #         logger.log_message('INFO', 'TRASH Folder Created Successfully')
            #     except OSError as e:
            #         logger.log_message('ERROR', 'Error while creating TRASH Folder, Check '
            #                                     'Permission')
            #         completed = False

            if 'upload' not in content_list:
                try:
                    os.mkdir(self.upload_data_path)
                    logger.log_message('INFO', 'UPLOAD Folder Created Successfully')
                except OSError as e:
                    logger.log_message('ERROR', 'Error while creating UPLOAD Folder, Check '
                                                'Permission')
                    completed = False
        return completed

    def check_top_folders_tree(self):
        content_list = []
        completed = True
        with os.scandir(self.root_path) as folders:
            for folder in folders:
                content_list.append(folder.name)

            if 'data' not in content_list:
                try:
                    os.mkdir('data/')
                    os.mkdir('data/logs')  # required to initiate logger

                except OSError as e:
                    completed = False
            else:
                for root, dirs, files in os.walk("data", topdown=True):
                    for name in dirs:
                        content_list.append(name)

                    if 'logs' not in content_list:
                        try:
                            os.mkdir(self.logs_data_path)

                        except OSError as e:
                            completed = False

        return completed

    def check_data_extract(self, today, yesterday):
        file_to_look_for_today = f'contacts_{today.strftime("%Y%m%d")}.csv'
        file_to_look_for_yesterday = f'contacts_{yesterday.strftime("%Y%m%d")}.csv'
        file_found = ''
        with os.scandir(self.data_extract_path) as files:
            for file in files:
                if file_to_look_for_today == file.name or file_to_look_for_yesterday == file.name:
                    db_extract_date = datetime.strptime(file.name.split('_')[1].split('.')[0], '%Y%m%d').date()
                    if today == db_extract_date:  # If yo found today data, return it immediately
                        return file.name
                    elif yesterday == db_extract_date:  # if you found yesterday data, keep looking for today version
                        file_found = file.name
        return file_found  # return yesterday if found or '' since you couldn't find today or yesterday

    def clear_folder_content(self, folder_name, logger):
        completed = True
        if folder_name == 'DELETE':
            with os.scandir(self.delete_data_path) as files:
                for file in files:
                    try:
                        os.remove(file)
                        logger.log_message('INFO', f'Successfully deleted \'{file.name}\' from DELETE Folder')

                    except OSError as e:
                        logger.log_message('ERROR', f'Error while clearing DELETE Folder content {e}')
                        completed = False

            return completed

        elif folder_name == 'UPLOAD':
            with os.scandir(self.upload_data_path) as files:
                for file in files:
                    try:
                        os.remove(file)
                        logger.log_message('INFO', f'Successfully deleted \'{file.name}\' from UPLOAD Folder')

                    except OSError as e:
                        logger.log_message('ERROR', f'Error while clearing UPLOAD Folder content {e}')
                        completed = False

            return completed

        elif folder_name == 'DB_EXTRACT':
            with os.scandir(self.data_extract_path) as files:
                for file in files:
                    try:
                        os.remove(file)
                        logger.log_message('INFO', f'Successfully deleted \'{file.name}\' from DB_EXTRACT/PROD Folder')

                    except OSError as e:
                        logger.log_message('ERROR', f'Error while clearing DB_EXTRACT/PROD Folder content {e}')
                        completed = False

            return completed

        elif folder_name == 'TRASH':
            with os.scandir(self.trash_data_path) as files:
                for file in files:
                    try:
                        os.remove(file)
                        logger.log_message('INFO', f'Successfully deleted \'{file.name}\' from TRASH Folder')

                    except OSError as e:
                        logger.log_message('ERROR', f'Error while clearing TRASH Folder content {e}')
                        completed = False

            return completed

        elif folder_name == 'RETRY':
            with os.scandir(self.retry_data_path) as files:
                for file in files:
                    try:
                        os.remove(file)
                        logger.log_message('INFO', f'Successfully deleted \'{file.name}\' from RETRY Folder')

                    except OSError as e:
                        logger.log_message('ERROR', f'Error while clearing RETRY Folder content {e}')
                        completed = False

            return completed
        else:
            logger.log_message('ERROR', f'Folder provided \" {folder_name} \"was not found!')
            return False

    def clear_error_folder(self, last_three_days):
        # print(last_three_days)
        completed = True
        with os.scandir(self.error_data_path) as files:
            for file in files:
                date_str = time.strftime("%d-%m-%Y", time.localtime(os.path.getmtime(file)))
                # print(date_str)
                if date_str not in last_three_days:
                    try:
                        os.remove(file)

                    except OSError as e:
                        completed = False

        return completed

    def clear_old_logs(self, last_seven_days):
        completed = True
        with os.scandir(self.logs_data_path) as files:
            for file in files:
                if file.name not in last_seven_days:
                    try:
                        os.remove(file)

                    except OSError as e:
                        completed = False

        return completed

    def move_source_to_destination(self, file_name, src_folder, des_folder, logger):
        source = self.folders[src_folder] + '/' + file_name
        destination = self.folders[des_folder] + '/' + file_name
        # print(source)
        # print(destination)

        try:
            os.rename(source, destination)
            logger.log_message('INFO', f'Successfully Moved file from {source} to {destination}')
            return True
        except OSError as e:
            logger.log_message('ERROR', f'Error while moving file {source} to {destination} {e}')
            return False

    def generate_contacts_keywords_from_delete_csv(self, logger):
        contacts_list = []
        # contact = {
        #     'search_keywords_list': [],
        #     'first_name': '',
        #     'last_name': ''
        # }

        try:
            with open(self.delete_data_path + '/deleted.csv', 'r') as read_obj:
                # pass the file object to reader() to get the reader object
                csv_reader = csv.DictReader(read_obj)
                # Iterate over each row in the csv using reader object
                for row in csv_reader:
                    entry = []   # clear list of keywords for each row (user)
                    contact = {  # clear contact information for each row (user)
                        'search_keywords_list': [],
                        'first_name': '',
                        'last_name': ''
                    }
                    # row variable is a list that represents a row in delete.csv (get user's full name)
                    if 'Contact First Name' in row:
                        if row['Contact First Name'] != '':
                            contact['first_name'] = row['Contact First Name']

                    if 'Contact Last Name' in row:
                        if row['Contact Last Name'] != '':
                            contact['last_name'] = row['Contact Last Name']

                    # row variable is a list that represents a row in delete.csv (get Keywords for contact)
                    if 'Contact Address 1' in row:
                        if row['Contact Address 1'] != '':
                            entry.append(row['Contact Address 1'])

                    if 'Contact Personal Email' in row:
                        if row['Contact Personal Email'] != '':
                            entry.append(row['Contact Personal Email'])

                    if 'Contact Work Email' in row:
                        if row['Contact Work Email'] != '':
                            entry.append(row['Contact Work Email'])

                    if 'Contact Home Phone' in row:
                        if row['Contact Home Phone'] != '':
                            entry.append(row['Contact Home Phone'])

                    if 'Contact Work Phone' in row:
                        if row['Contact Work Phone'] != '':
                            entry.append(row['Contact Work Phone'])

                    if 'Contact Cell Phone' in row:
                        if row['Contact Cell Phone'] != '':
                            entry.append(row['Contact Cell Phone'])

                    contact['search_keywords_list'] = entry

                    contacts_list.append(contact)
                    # print(entry)
                # print(search_keywords_list)
                logger.log_message('INFO', f'Successfully created keywords list for contacts in delete.csv')
                return contacts_list

        except OSError as e:
            if e.errno == 2:
                logger.log_message('INFO', f'There is no contacts to be deleted {e}')
            else:
                logger.log_message('ERROR', f'Error while Opening deleted.csv {e}')
            return contacts_list

# def main():
#     fm = FilesManager()
#     print(fm.clear_error_folder(['2020-06-18']))
#
#
# if __name__ == "__main__":
#     main()
