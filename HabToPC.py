"""__author__ = "Ghaith Rawi"
__copyright__ = "Copyright (C) 2020 Datacom"
__license__ = "Private Use - Restricted"
__version__ = "1.0"
-- Last update : 18-6-2020
"""
from logger import Logger
from PureCloud import PC
import CSVProcessor
import time
from datetime import datetime, timedelta
from FilesManager import FilesManager

# initiate Objects
start_time = time.time()  # Calculate Execution time
pureCloud = PC()
Logger = Logger()
FM = FilesManager()

# Config values
file_uploading_retry_limit = 1

# Generate Date values
today = datetime.now().date()
today_date_str = str(today.day) + '/' + str(today.month) + '/' + str(today.year)

yesterday_date_str = ''
yesterday = today - timedelta(days=1)
yesterday_date_str = str(yesterday.day) + '/' + str(yesterday.month) + '/' + str(yesterday.year)
# Logs Dates _ last week including Today
last_seven_days_list = []
for i in range(0, 7):
    last_seven_days_list.append((today - timedelta(days=i)).strftime("%d-%m-%Y") + '.log')

# Three days date _ For files to be kept in Error Folder (delete any files older than 3 days)
last_three_days_list = []
for i in range(0, 3):
    last_three_days_list.append((today - timedelta(days=i)).strftime("%d-%m-%Y"))

if today_date_str == '' or yesterday_date_str == '':
    print('ERROR', 'Date value was not generated successfully, Check System Date setting')

else:
    # Check Top Folders Tree (data & data/logs)
    if FM.check_top_folders_tree():
        # initiate logger only after you create data folder as logging is in data/logs (Folder)
        Logger.start_logger(today)
        Logger.log_message("INFO", '\n--Logger Started Successfully--\n')
        # Check Child folders. Create all missing folders
        if FM.create_folders_tree(Logger):
            Logger.log_message("INFO", 'All required folders are existed')

        # Check if Data was extracted from DB (only process data for today or yesterday. File Format : contacts_YYYYMMDD
        # Csv File name will be returned only if a file date is today or yesterday
        data_extract_file = FM.check_data_extract(today, yesterday)
        if data_extract_file != '':

            files_list_to_upload = []
            files_list_to_retry = []
            data_extract_file = f'data/db_extract/PROD/{data_extract_file}'
            files_list_to_upload = CSVProcessor.process_main_csv(data_extract_file, yesterday_date_str, Logger)

            # if len(files_list_to_upload) != 0:  # Upload Contact files to PC
            #     auth_token = pureCloud.get_pc_token(Logger)[0]
            #     # print(auth_token)
            #     if auth_token != 'error':
            #         for contact_file in files_list_to_upload:
            #             if not pureCloud.bulk_upload_external_contacts_csv(contact_file, auth_token, Logger):
            #                 # Move all Failed files to retry list
            #                 files_list_to_retry.append(contact_file)
            #         # When all is finished, check if there is any files to retry uploading to PC
            #         if len(files_list_to_retry) != 0:
            #             Logger.log_message("INFO", 'Retrying to upload failed contact files')
            #             Logger.log_message("INFO", f'Retry List {files_list_to_retry}')
            #             Logger.log_message("INFO", f'Configured Retry Limit =  {file_uploading_retry_limit}')
            #             # Retry uploading files to PC based on Max retry Limit
            #             for retry in range(0, file_uploading_retry_limit):
            #                 for contact_file_index in range(0, len(files_list_to_retry)):
            #                     if pureCloud.bulk_upload_external_contacts_csv(files_list_to_retry[0], auth_token,
            #                                                                    Logger):
            #                         files_list_to_retry.remove(files_list_to_retry[0])  # remove successful Files
            #                         # from list
            #                     else:
            #                         Logger.log_message("ERROR",
            #                                            f'Retrying to upload \'{files_list_to_retry[0]}\' to PC failed')

            #         # After finishing all retries, move any failed files to Error folder
            #         if len(files_list_to_retry) != 0:
            #             for contact_file_index in range(0, len(files_list_to_retry)):
            #                 if FM.move_source_to_destination(files_list_to_retry[0], 'upload', 'error', Logger):
            #                     del files_list_to_retry[0]  # remove successful Files from list

            #         # Now check if there is any Contacts which need to be removed from Purecloud (Delete Folder)
            #         pureCloud.delete_contacts(auth_token, Logger)

            #         # When all is done, Clear all folders to be ready for new data (Next DB extract)
            #         if FM.clear_folder_content('UPLOAD', Logger):
            #             Logger.log_message('INFO', 'Successfully cleared UPLOAD Folder content')

            #         FM.clear_folder_content('DELETE', Logger)
            #         FM.clear_folder_content('DB_EXTRACT', Logger)

            #         # Make sure to clean old files in Error folder (anything older than 3 days)
            #         if FM.clear_error_folder(last_three_days_list):
            #             Logger.log_message("INFO", 'Folder (ERROR) was cleaned, deleted files more than 3 days old')

            #         # Make sure to clean old logs before exiting
            #         if FM.clear_old_logs(last_seven_days_list):
            #             Logger.log_message("INFO", 'Folder (LOGS) was cleaned, deleted logs more than 7 days old')

            #     else:  # if Token was not generated, clear Upload folder
            #         if FM.clear_folder_content('UPLOAD', Logger):
            #             Logger.log_message('INFO', 'Successfully cleared UPLOAD Folder content')

            # else:
            #     Logger.log_message("ERROR", 'No Contacts Files to upload, check \"upload\" Folder content')
        else:
            Logger.log_message("ERROR", 'No Data extract was found in DB_EXTRACT/PROD Folder')
    else:
        print('Error creating Top folders Tree, Check Permission')


elapsed_time_secs = time.time() - start_time
msg = "Execution Finished in %s seconds" % timedelta(seconds=round(elapsed_time_secs))
Logger.log_message("INFO", msg)
